﻿using MenuInterpreter.Parser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InteractiveTestApp
{
	class Program
	{
		private static string _exampleFilePath = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\..\\data\\script_example.yaml");

		static void Main(string[] args)
		{
			// create parser
			var parser = new YamlMenuParser();
			// create interpreter
			var interpreter = parser.CreateInterpreterFromFile(_exampleFilePath);

			// register checkpoint handlers
			interpreter.RegisterCheckpointHandler("HasKilled", (sender, a) =>
			{
				Console.WriteLine($"Have you killed {a.Amount} {a.Value}? (1 - yes, 0 - no)");
				int answer = Convert.ToInt32(Console.ReadLine());
				a.Result = answer == 1;
			});

			interpreter.RegisterCheckpointHandler("HasItem", (sender, a) =>
			{
				a.Result = true;
			});

			// start
			interpreter.Start();

			while (interpreter.IsFinished == false)
			{
				var currentStep = interpreter.GetCurrentStep();
				Console.WriteLine($"Current step: {currentStep.Text}");

				if (currentStep.Answers.Any())
				{
					foreach (var ans in currentStep.Answers)
					{
						Console.WriteLine($"{ans.Id}. {ans.Text}");
					}

					int answer = Convert.ToInt32(Console.ReadLine());

					interpreter.Move(answer);
				}

				Console.WriteLine("======");
			}

			Console.WriteLine("Finished");

			var history = interpreter.GetHistory();
			foreach (var h in history)
			{
				Console.WriteLine($"step {h.ItemId} answered {h.AnswerId}");
			}

			Console.ReadKey();
		}
	}
}
