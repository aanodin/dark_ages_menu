﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.IO;

namespace MenuInterpreter.Tests
{
	/// <summary>
	/// Summary description for InterpreterTests
	/// </summary>
	[TestClass]
	public class InterpreterTests
	{
		private List<MenuItem> _items;
		private List<MenuItem> _itemsWithCheckpoint;

		public InterpreterTests()
		{
			_items = new List<MenuItem>()
			{

				new MenuItem(1, MenuItemType.Step, "First step", new Answer[]
				{
					new Answer(1, "next", 2)
				}),
				new MenuItem(2, MenuItemType.Step, "Second step", new Answer[]
				{
					new Answer(1, "next", 3)
				}),
				new MenuItem(3, MenuItemType.Step, "Third step", new Answer[]
				{
					new Answer(1, "next")
				})
			};
			

			_itemsWithCheckpoint = new List<MenuItem>()
			{
				new MenuItem(1, MenuItemType.Step, "First step", new Answer[]
				{
					new Answer(1, "next", 2)
				}),
				new CheckpointMenuItem(2, "CheckpointType", new Answer[]
				{
					new Answer(Constants.CheckpointOnSuccess, "go back", 1),
					new Answer(Constants.CheckpointOnFail, "finish")
				})
			};
		}


		[TestMethod]
		public void Interpreter_moves_to_start_item()
		{
			// arrange
			var interp = new Interpreter(_items, _items.First());

			// act
			interp.Start();

			// assert
			Assert.AreEqual(_items.First(), interp.GetCurrentStep());
		}

		[TestMethod]
		public void Interpreter_moves_to_next_item()
		{
			// arrange
			var interp = new Interpreter(_items, _items.First());

			// act
			interp.Start();
			interp.Move(1);

			// assert
			Assert.AreEqual(_items[1], interp.GetCurrentStep());
		}

		[TestMethod]
		public void Interpreter_moves_to_finish_item()
		{
			// arrange
			var interp = new Interpreter(_items, _items.First());

			// act
			interp.Start();
			interp.Move(1);
			interp.Move(1);
			interp.Move(1);

			// assert
			Assert.AreEqual(null, interp.GetCurrentStep());
			Assert.IsTrue(interp.IsFinished);
		}

		[TestMethod]
		public void Interpreter_goes_back_when_menu_closed()
		{
			// arrange
			var items = new List<MenuItem>()
			{
				new MenuItem(1, MenuItemType.Step, "First step", new Answer[]
				{
					new Answer(1, "next", 2)
				}),
				new MenuItem(2, MenuItemType.Menu, "Second step", new Answer[]
				{
					new Answer(1, "finish")
				})
			};

			var interp = new Interpreter(items, items.First());

			// act
			interp.Start();
			interp.Move(1);
			interp.Move(-2);
			var currentStep = interp.GetCurrentStep();

			// assert
			Assert.AreEqual(false, interp.IsFinished);
			Assert.AreEqual(1, currentStep.Id);
		}

		[TestMethod]
		public void Interpreter_calls_checkpoint_handler()
		{
			// arrange			
			bool called = false;
		
			var interp = new Interpreter(_itemsWithCheckpoint, _itemsWithCheckpoint.First());
			interp.RegisterCheckpointHandler("CheckpointType", (sender, args) =>
			{
				called = true;
			});

			// act
			interp.Start();
			interp.Move(1);

			// assert
			Assert.IsTrue(called);
		}

		[TestMethod]
		public void Interpreter_calls_checkpoint_handler_on_success()
		{
			// arrange
			var interp = new Interpreter(_itemsWithCheckpoint, _itemsWithCheckpoint.First());
			interp.RegisterCheckpointHandler("CheckpointType", (sender, args) =>
			{
				args.Result = true;
			});

			// act
			interp.Start();
			interp.Move(1);

			// assert
			Assert.AreEqual(_itemsWithCheckpoint.First(), interp.GetCurrentStep());
			Assert.IsFalse(interp.IsFinished);
		}

		[TestMethod]
		public void Interpreter_calls_checkpoint_handler_on_fail()
		{
			// arrange
			var interp = new Interpreter(_itemsWithCheckpoint, _itemsWithCheckpoint.First());
			interp.RegisterCheckpointHandler("CheckpointType", (sender, args) =>
			{
				args.Result = false;
			});

			// act
			interp.Start();
			interp.Move(1);

			// assert
			Assert.AreEqual(null, interp.GetCurrentStep());
			Assert.IsTrue(interp.IsFinished);
		}

		[TestMethod]
		public void Interpreter_can_be_restarted()
		{
			// arrange
			var startItem = _items.First();
			var interp = new Interpreter(_items, startItem);

			interp.Start();

			Assert.AreEqual(startItem, interp.GetCurrentStep());
			Assert.IsFalse(interp.IsFinished);
			interp.Move(1);
			interp.Move(1);
			interp.Move(1);

			Assert.AreEqual(null, interp.GetCurrentStep());
			Assert.IsTrue(interp.IsFinished);

			// act
			interp.Start();

			// assert
			Assert.AreEqual(startItem, interp.GetCurrentStep());
			Assert.IsFalse(interp.IsFinished);

			interp.Move(1);
			interp.Move(1);
			interp.Move(1);

			Assert.AreEqual(null, interp.GetCurrentStep());
			Assert.IsTrue(interp.IsFinished);
		}

		[TestMethod]
		public void Interpreter_doesnt_call_checkpoint_until_started()
		{
			// arrange
			// having items with start on checkpoint
			var items = new List<MenuItem>()
			{
				new CheckpointMenuItem(1, "CheckpointType", new Answer[]
				{
					new Answer(Constants.CheckpointOnSuccess, "go back", 2),
					new Answer(Constants.CheckpointOnFail, "finish")
				}),
				new MenuItem(2, MenuItemType.Step, "First step", new Answer[]
				{
					new Answer(1, "next", 2)
				})				
			};

			bool called = false;

			// act
			var interp = new Interpreter(items, items.First());
			interp.RegisterCheckpointHandler("CheckpointType", (sender, a) =>
			{
				called = true;
				a.Result = true;
			});

			// assert
			Assert.IsFalse(called);

			interp.Start();
			Assert.IsTrue(called);
			Assert.AreEqual(items.Last(), interp.GetCurrentStep());
		}
	}
}
