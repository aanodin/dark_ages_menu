﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MenuInterpreter.Tests
{
	[TestClass]
	public class MenuItemTests
	{
		[TestMethod]
		public void MenuItem_created_correctly()
		{
			// arrange
			// act
			var item = new MenuItem(23, MenuItemType.Step, "my text", new Answer[]
			{
				new Answer(1, "first", 2),
				new Answer(2, "second", 3)
			});

			// assert
			Assert.AreEqual(23, item.Id);
			Assert.AreEqual("my text", item.Text);
			Assert.AreEqual(2, item.Answers.Length);
		}

		[TestMethod]
		public void MenuItem_returns_correct_next_item_id()
		{
			// arrange
			var item = new MenuItem(23, MenuItemType.Step, "my text", new Answer[]
			{
				new Answer(1, "first", 2),
				new Answer(2, "second", 3)
			});

			var expectedId = 3;

			// act
			var actualId = item.GetNextItemId(2);

			// assert
			Assert.AreEqual(expectedId, actualId);
		}

		[TestMethod]
		public void MenuItem_creates_closing_answer_for_menus()
		{
			// arrange
			// act
			var menu = new MenuItem(1, MenuItemType.Menu, "hello", null);

			// assert
			Assert.IsNotNull(menu.Answers);
			Assert.AreEqual(1, menu.Answers.Length);

			var answer = menu.Answers.First();
			Assert.AreEqual(Constants.MenuCloseLink, answer.Id);
			Assert.AreEqual(Constants.NoLink, answer.LinkedId);
		}

		[TestMethod]
		public void MenuItem_doesnt_create_closing_answer_for_step()
		{
			// arrange
			// act
			var menu = new MenuItem(1, MenuItemType.Step, "hello", null);

			// assert
			Assert.IsNotNull(menu.Answers);
			Assert.AreEqual(0, menu.Answers.Length);
		}

		[TestMethod]
		public void MenuItem_always_has_initialized_answers()
		{
			// arrange
			// act
			var menu = new MenuItem(1, MenuItemType.Step, "hello", null);

			// assert
			Assert.IsNotNull(menu.Answers);
		}
	}
}
