﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MenuInterpreter.Parser;
using System.IO;

namespace MenuInterpreter.Tests
{
	/// <summary>
	/// Summary description for YamlParserTest
	/// </summary>
	[TestClass]
	public class YamlParserTest
	{
		private static string _exampleFilePath = Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\..\\data\\script_example.yaml"); 

		[TestMethod]
		public void YamlMenuParser_parses_text_files()
		{
			// arrange 
			var parser = new YamlMenuParser();

			// act
			var result = parser.Parse(_exampleFilePath);

			// assert
			Assert.IsNotNull(result);
			Assert.AreEqual(3, result.sequences.Count);
			Assert.AreEqual(2, result.menus.Count);
			Assert.AreEqual(3, result.checkpoints.Count);
		}

		[TestMethod]
		public void YamlMenuParser_creates_interpreter()
		{
			// arrange 
			var parser = new YamlMenuParser();

			// act 
			var interp = parser.CreateInterpreterFromFile(_exampleFilePath);

			// assert
			Assert.IsNotNull(interp);
		}
	}
}
